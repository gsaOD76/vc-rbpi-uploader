#include "Counter.h"

#include <list>
#include <vector>
#include <stdexcept>
#include <sstream>
#include <iomanip>

#include <boost/algorithm/string.hpp>
#include <boost/regex.hpp>
#include <boost/current_function.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/range/algorithm_ext/erase.hpp>
#include <boost/uuid/sha1.hpp>


/*static*/
std::string
Counter::
getSha1(const std::string& arg)
{
    boost::uuids::detail::sha1 sha1;
    sha1.process_bytes(arg.data(), arg.size());
    unsigned int hash[5] = {0,0,0,0,0};
    sha1.get_digest(hash);
    
    std::ostringstream buf;
    
    for(int i = 0; i < 5; ++i)
        buf << std::hex << std::setfill('0') << std::setw(8) << hash[i];
    
    return buf.str();
}

std::string 
Counter::
ptimeToStrFormat(boost::posix_time::ptime now, std::string format)
{   
	std::stringstream ss;
	
	std::locale loc(ss.getloc(), new boost::posix_time::time_facet(format.c_str()));
	
    ss.imbue(loc);	
    ss << now;
    
	return ss.str();
}

void
Counter::
getIP()
{
    try 
    {
        boost::asio::io_service netService;
        boost::asio::ip::udp::resolver   resolver(netService);
        boost::asio::ip::udp::resolver::query query(boost::asio::ip::udp::v4(), "google.com", "");
        boost::asio::ip::udp::resolver::iterator endpoints = resolver.resolve(query);
        boost::asio::ip::udp::endpoint ep = *endpoints;
        boost::asio::ip::udp::socket socket(netService);
        //socket.set_option(rcv_timeout_option{ 2000 });
        
        socket.connect(ep);
        
        boost::asio::ip::address addr = socket.local_endpoint().address();
        
        std::cout << "Current IP according to google is: " << addr.to_string() << std::endl;
        
        _ip = addr.to_string();
    } 
    catch (std::exception& e)
    {
        std::cerr << boost::posix_time::to_iso_extended_string(_currentTime) 
                  << " Could not deal with socket. Exception: " << e.what() << std::endl;
    }  
 }

void
Counter::
init(const std::string &serial, const std::string &salt, const boost::posix_time::ptime &dt)
{
    _counterSerial = serial;
    _signSalt = salt;
    _currentTime = dt;
}

/**
+BCNT:1,9958,36,0,99,178,145,159,232
+BCNT:2,bfb2,0,0,720,0,0,0,0
*/
bool 
Counter::
parseBlocks(std::string &data)
{
    std::cout << BOOST_CURRENT_FUNCTION << " data : " << data << std::endl;
            
    std::list<std::string> countersList;
    
    boost::split(countersList, data, boost::is_any_of("\n"));
    
    for (auto it = countersList.begin(); it != countersList.end(); ++it)
    {
        std::string counterDataStr = *it;
        
        counterDataStr = boost::erase_first_copy(counterDataStr, "+BCNT:");        
        counterDataStr = boost::remove_erase_if(counterDataStr, boost::is_any_of("\n\r"));
                
        boost::regex expr{"^\\d{1},[\\dabcdefABCDEF]{1,4},(\\d{1,3},){6}\\d{1,3}$"};
        
        if (!regex_match(counterDataStr, expr))
            continue;
           
        std::vector<std::string> counterData;
        boost::split(counterData, counterDataStr, boost::is_any_of(","));
                
        if (counterData.size() != 9)
            continue;
        
        DoorBlockInfo tmpInfo;
        
        try
        {
            tmpInfo.bindIndex = std::stoi(counterData.at(0));
            tmpInfo.uid = counterData.at(1);
            tmpInfo.count = std::stoi(counterData.at(2));
            tmpInfo.state = std::stoi(counterData.at(3));
            tmpInfo.waitCounter = std::stoi(counterData.at(4));
            tmpInfo.batCurent = std::stoi(counterData.at(5));
            tmpInfo.batCritical = std::stoi(counterData.at(6));
            tmpInfo.batLow = std::stoi(counterData.at(7));
            tmpInfo.batNew = std::stoi(counterData.at(8));
            
            _currentCount += tmpInfo.count;
        }
        catch (const std::invalid_argument& ia) 
        {
            std::cerr << boost::posix_time::to_iso_extended_string(_currentTime) 
                      << "Invalid argument: " << ia.what() << std::endl;
        }
        
        _info.push_back(tmpInfo);
    }
    
    std::cout << BOOST_CURRENT_FUNCTION << " Block parsed : " << _info.size() 
                                        << " , Current count : " << _currentCount << std::endl;
    
    return true;
}

/**
+GMR:1.0,2
*/
bool 
Counter::
parseVersion(std::string &data)
{
    std::cout << BOOST_CURRENT_FUNCTION << " data : " << data << std::endl;
            
    std::vector<std::string> counterInfo;
    
    std::string counterInfoStr = data;
        
    counterInfoStr = boost::erase_first_copy(counterInfoStr, "+GMR:");        
    counterInfoStr = boost::remove_erase_if(counterInfoStr, boost::is_any_of("\n\r"));
                
    boost::regex expr{"^\\d{1}\\.\\d{1},\\d{1}$"};
        
    if (!boost::regex_match(counterInfoStr, expr))
        return false;
           
    boost::split(counterInfo, counterInfoStr, boost::is_any_of(","));
                
    if (counterInfo.size() != 2)
        return false;
            
    try
    {
        _counterVersion = counterInfo.at(0);
        _counterMcuStatusReg = std::stoi(counterInfo.at(1));
    }
    catch (const std::invalid_argument& ia) 
    {
        std::cerr << boost::posix_time::to_iso_extended_string(_currentTime) 
                  << " Invalid argument: " << ia.what() << std::endl;
    }
    
    std::cout << BOOST_CURRENT_FUNCTION << " Counter ver : " << _counterVersion
                                        << " , MCU Status reg : " << _counterMcuStatusReg << std::endl;
    
    return true;
}

/**
{
	"type": "count_rbpi",
	"serial": "0309",
	"ver": "1.0",
	"rtc": "2019-06-07T00:31:13",
	"door_blocks": [{
		"index": "1",
		"uid": "9958",
		"count": "195",
		"state": "0",
		"wait_counter": "200",
		"bat_state": ["177", "145", "159", "232"]
	}, {
		"index": "2",
		"uid": "bfb2",
		"count": "0",
		"state": "0",
		"wait_counter": "720",
		"bat_state": ["0", "0", "0", "0"]
	}],
	"count_data": [{
		"date_time": "2019-06-07T00:31:13",
		"count": "195"
	}]
}
*/
void 
Counter::
makeRequestJSON(std::string &data)
{
    boost::property_tree::ptree root;
    
    std::stringstream signedData;
    
    unsigned int amountCount = 0;
    
    std::string formatISO("%Y-%m-%dT%H:%M:%S");
    std::string formatZeroSec("%Y-%m-%dT%H:%M:00");
    
    
    root.put("type", "count_rbpi");
    root.put("serial", _counterSerial);
    root.put("ver", _counterVersion);
    root.put("mcu_status_reg", _counterMcuStatusReg);
    root.put("ip", _ip);
    
    std::string strTime = ptimeToStrFormat(_currentTime, formatISO);
    
    root.put("rtc", strTime);
    
    signedData << "count_rbpi" << _counterSerial << _counterVersion << _counterMcuStatusReg << _ip << strTime;
    
    boost::property_tree::ptree doorBlocksArray;
    
    for (auto it = _info.begin(); it != _info.end(); ++it)
    {
        DoorBlockInfo tmpInfo = *it;
        
        boost::property_tree::ptree doorBlockObj;
        
        boost::property_tree::ptree batStateArray;
        boost::property_tree::ptree cellCurrent, cellCritical, cellLow, cellNew;
        
        doorBlockObj.put("index", tmpInfo.bindIndex);
        doorBlockObj.put("uid", tmpInfo.uid);
        doorBlockObj.put("count", tmpInfo.count);
        doorBlockObj.put("state", tmpInfo.state);
        doorBlockObj.put("wait_counter", tmpInfo.waitCounter);
        
        cellCurrent.put_value(tmpInfo.batCurent);
        batStateArray.push_back(make_pair("", cellCurrent));
        
        cellCritical.put_value(tmpInfo.batCritical);
        batStateArray.push_back(make_pair("", cellCritical));
        
        cellLow.put_value(tmpInfo.batLow);
        batStateArray.push_back(make_pair("", cellLow));
        
        cellNew.put_value(tmpInfo.batNew);
        batStateArray.push_back(make_pair("", cellNew));
        
        doorBlockObj.add_child("bat_state", batStateArray);
        
        doorBlocksArray.push_back(make_pair("", doorBlockObj));
        
        signedData << tmpInfo.bindIndex << tmpInfo.uid << tmpInfo.count << tmpInfo.state << tmpInfo.waitCounter;
        
        signedData << tmpInfo.batCurent << tmpInfo.batCritical << tmpInfo.batLow << tmpInfo.batNew;
          
    }

    root.add_child("door_blocks", doorBlocksArray);
    
    boost::property_tree::ptree countDataArray;
    
    //std::string format("%Y-%m-%dT%H:%M:00");
    
    for (auto it = _countRecordsArray.begin(); it != _countRecordsArray.end(); ++it)
    {
        CountRecord tmpRecord = *it;
        
        boost::property_tree::ptree countRecordObj;
        
        countRecordObj.put("date_time", ptimeToStrFormat(tmpRecord._timePoint, formatZeroSec));
        countRecordObj.put("count", tmpRecord._count);
        
        amountCount += tmpRecord._count;
        
        countDataArray.push_back(std::make_pair("", countRecordObj));
    }
    
    signedData << amountCount;
                      
    root.add_child("count_data", countDataArray);
    
    signedData << _signSalt;
    
    //root.put("signed_data", signedData.str());
            
    root.put("hash", getSha1(signedData.str()));
    
    std::ostringstream buf;
    
    write_json(buf, root, false);
    
    buf << std::endl;
    
    data = buf.str();
}

void
Counter::
pushCurrentCount()
{
    CountRecord tmp(_currentTime, _currentCount);
    _countRecordsArray.push_back( tmp);    
}

void
Counter::
sortCount()
{
    boost::range::sort(_countRecordsArray); 
}

void 
Counter::
Print() const
{   
    for (auto it = _countRecordsArray.begin(); it != _countRecordsArray.end(); ++it)
    {
        std::cout << "DateTime : " << to_iso_extended_string((*it)._timePoint) 
                  << ", count : " << (*it)._count << std::endl;
    }
}

void 
Counter::
checkSystemTime(const std::string &dateTime)
{
    std::cout << "Server time string : " << dateTime << std::endl;
    
    std::tm t;
    
    //2019-07-22T22-30-01
    std::istringstream ss(dateTime);
    
    ss >> std::get_time(&t, "%Y-%m-%dT%H-%M-%S");
    
    if (ss.fail()) 
    {
        std::cerr << boost::posix_time::to_iso_extended_string(_currentTime) 
                  << " Parse server time failed" << std::endl;
        return;
    } 
    else 
    {
        std::cout << "Server time converted : " << std::put_time(&t, "%c") << std::endl;
    }
    
    boost::posix_time::ptime serverTimePosix = boost::posix_time::ptime_from_tm(t);
    
    std::cout << "Server time posix : "  << boost::posix_time::to_iso_extended_string(serverTimePosix) << std::endl;
    
    boost::posix_time::time_duration diff = serverTimePosix - _currentTime;
    
    boost::posix_time::time_duration::sec_type x = diff.total_seconds();
    
    int diffSecAbs = std::abs(x);
    
    std::cout << "Diff time, sec : " << diffSecAbs << std::endl;
    
    if (diffSecAbs > TIME_MAX_DIFF_SEC)
    {
         std::cerr << boost::posix_time::to_iso_extended_string(_currentTime) 
                   << " Out of sync system time and server time" << std::endl;
    }
}
