#ifndef COUNTER_H
#define	COUNTER_H

#include <string>
#include <utility> 
#include <vector>

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/posix_time_io.hpp>
#include <boost/property_tree/json_parser.hpp>


#include <iostream>
#include <fstream>
#include <boost/serialization/vector.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/date_time/posix_time/time_serialize.hpp>

#include <boost/range/algorithm.hpp>
#include <boost/asio.hpp>

#define TIME_MAX_DIFF_SEC  30


class Counter
{
public:
    
    void init(const std::string &serial, const std::string &salt, const boost::posix_time::ptime &dt);
    
    void getIP();
    
    void checkSystemTime(const std::string &dateTime);
    
    Counter() :
        _currentCount(0),
        _currentTime(boost::date_time::not_a_date_time)
    {}
    
    typedef std::pair<boost::posix_time::ptime, unsigned int> CountRecordType; 
 
    struct DoorBlockInfo
    {
       unsigned int bindIndex;
       std::string  uid;
       unsigned int count;
       unsigned int state;
       unsigned int waitCounter;
       unsigned int batCurent;
       unsigned int batCritical;
       unsigned int batLow;
       unsigned int batNew;
    
       DoorBlockInfo() :
          bindIndex(0),
          uid(""),
          count(0),
          state(0),
          waitCounter(0),
          batCurent(0),
          batCritical(0),
          batLow(0),
          batNew(0)
       {}
    };
    
    struct CountRecord
    {
        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive & ar, const unsigned int version)
        {
            ar & _timePoint;
            ar & _count;
        }
        
        boost::posix_time::ptime _timePoint;
        unsigned int _count;
        
        CountRecord(const boost::posix_time::ptime & tp, const unsigned int & cnt) :
            _timePoint(tp), 
            _count(cnt)
        {}
        
        CountRecord(){}
        
        bool operator <(const CountRecord& rhs)
        {
            return this->_timePoint < rhs._timePoint;
        }
    };
    
    bool parseBlocks(std::string &data);
    
    bool parseVersion(std::string &data);
    
    void makeRequestJSON(std::string &data);
    
    void pushCurrentCount();
    
    void sortCount();
        
    void Print() const;
    
    
private:
    
    BOOST_SERIALIZATION_SPLIT_MEMBER()
        
    friend class boost::serialization::access;

    template<class Archive>
    
    void save(Archive & ar, const unsigned int version) const
    {
      ar & _countRecordsArray;
    }
 
    template<class Archive>
    void load(Archive & ar, const unsigned int version) 
    {
      ar & _countRecordsArray;
    }
    
    static std::string getSha1(const std::string& p_arg);
    static std::string ptimeToStrFormat(boost::posix_time::ptime now, std::string format);

private:
    std::string _counterSerial;
    
    std::string _ip;
    
    std::string _counterVersion;
    
    unsigned int _counterMcuStatusReg;
    
    std::string _signSalt;
    
    unsigned int _currentCount;
    
    boost::posix_time::ptime _currentTime;
    
    std::vector<DoorBlockInfo> _info;
    
    std::vector<CountRecord> _countRecordsArray; 
};

#endif  //COUNTER_H
