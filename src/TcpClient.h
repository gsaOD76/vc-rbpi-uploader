#ifndef TCPCLIENT_H
#define	TCPCLIENT_H

#include <boost/asio/connect.hpp>
#include <boost/asio/deadline_timer.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/read_until.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/system/system_error.hpp>
#include <boost/asio/write.hpp>
#include <cstdlib>
#include <iostream>
#include <string>
#include <boost/lambda/bind.hpp>
#include <boost/lambda/lambda.hpp>

using boost::asio::deadline_timer;
using boost::asio::ip::tcp;
using boost::lambda::bind;
using boost::lambda::var;
using boost::lambda::_1;


class TcpClient
{
public:
    TcpClient();
    
    void connect(const std::string& host, const std::string& service,
                 boost::posix_time::time_duration timeout);
    
    std::string read_line(boost::posix_time::time_duration timeout);
    
    void write_line(const std::string& line, boost::posix_time::time_duration timeout);
    
private:
  void check_deadline();
  
  boost::asio::io_service io_service_;
  tcp::socket socket_;
  deadline_timer deadline_;
  boost::asio::streambuf input_buffer_;    
};

#endif  //TCPCLIENT_H
