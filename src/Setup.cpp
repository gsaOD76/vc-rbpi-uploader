#include "Setup.h"

#include <iostream>

using namespace std;

Setup::
Setup() 
{}


Setup::
Setup(const std::string& filename) :
    _setupFileName(filename)
{}

Setup::
~Setup() 
{}


void 
Setup::
read()
{
    boost::filesystem::path full_path(boost::filesystem::current_path());
    
    string inifilename(full_path.c_str());
    
    inifilename += boost::filesystem::path::preferred_separator;
    inifilename += _setupFileName;
    
    cout << "Load setup filename : " << inifilename << endl;

    boost::property_tree::ini_parser::read_ini(inifilename, _pt);    
}

void 
Setup::
parse()
{
    _serverHost = _pt.get<std::string>("SERVER.Host");
    _serverPort = _pt.get<std::string>("SERVER.Port");
    _serverSalt = _pt.get<std::string>("SERVER.PwdSalt");
    
    _counterPort = _pt.get<std::string>("COUNTER.Port");
    _counterSerial = _pt.get<std::string>("COUNTER.Serial");
    _baudRate = _pt.get<unsigned int>("COUNTER.BaudRate");

    cout << "*************************************" << endl;    
    cout << "SERVER.Host      : " << _serverHost << endl;
    cout << "SERVER.Port      : " << _serverPort << endl;
    cout << "COUNTER.Port     : " << _counterPort << endl;
    cout << "COUNTER.Serial   : " << _counterSerial << endl;
    cout << "COUNTER.BaudRate : " << _baudRate << endl;
    cout << "*************************************" << endl;
}
