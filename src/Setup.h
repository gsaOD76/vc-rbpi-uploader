#ifndef SETUP_H
#define	SETUP_H

#include <boost/utility.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/filesystem.hpp>

class Setup: private boost::noncopyable
{
public:
    Setup();
    
    Setup(const std::string& filename);
    
    ~Setup();
    
    void read();
    
    void parse();
    
    std::string getServerHost() const {return _serverHost;}
    std::string getServerPort() const {return _serverPort;}
    std::string getServerSalt() const {return _serverSalt;}
    
    std::string getCounterPort() const {return _counterPort;}
    std::string getCounterSerial() const {return _counterSerial;}
    unsigned int getCounterBaudRate() const {return _baudRate;}
    
private:
    
    std::string _setupFileName;
    
    boost::property_tree::ptree _pt;
    
    
    std::string _serverHost;
    std::string _serverPort;
    std::string _serverSalt;
    
    std::string _counterPort;
    std::string _counterSerial;
    unsigned int _baudRate;
};

#endif  //SETUP_H
