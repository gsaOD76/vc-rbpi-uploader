/*
 * File:   main.cpp
 * Author: Sergey Galchenko
 * Distributed under the Boost Software License, Version 1.0.
 */

#include "TimeoutSerial.h"
#include "Setup.h"
#include "Counter.h"
#include "TcpClient.h"

#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/range/algorithm_ext/erase.hpp>
#include <boost/filesystem.hpp>
#include <boost/system/error_code.hpp>
#include <boost/program_options.hpp>

#include <boost/asio.hpp> 


int main(int argc, char* argv[])
{
    boost::posix_time::ptime now = boost::posix_time::second_clock::local_time();
    
    std::cout << std::endl << std::endl 
              << boost::posix_time::to_iso_extended_string(now) << " STARTING..." << std::endl;
    
    TimeoutSerial serial;
    Counter counter;
    
    try 
    {   
        if (!boost::filesystem::exists("setup.ini" ))
        {
             std::cerr << boost::posix_time::to_iso_extended_string(now) 
                       << " Can't find INI file!" << std::endl;
             
             return boost::system::errc::no_such_file_or_directory;
        }
        
        Setup setupIni("setup.ini");
        
        setupIni.read();
        
        setupIni.parse();
        
        serial.open(setupIni.getCounterPort(), setupIni.getCounterBaudRate());
        
        serial.setTimeout(boost::posix_time::seconds(1));
        
        try
        {
            boost::program_options::options_description desc("Allowed options");
            desc.add_options()
                    ("help,h", "Show help")
                    ("unbind,u", "Unbind all door blocks")
                    ("reload,r", "Reload radio module")
                    ("info,i", "Get station info")
                    ("blocks,b", "Get blocks info");
            
            boost::program_options::variables_map vm;
            boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
            boost::program_options::notify(vm);
            
            if (vm.count("help")) 
            {
                std::cout << desc << "\n";
                return boost::system::errc::success;
            }
            else if (vm.count("info")) 
            {
                serial.writeString("AT+GMR\n");
                std::string answer = serial.readStringUntil("OK\n");
                std::cout << answer << std::endl;
                serial.close();
                return boost::system::errc::success;
            }
            else if (vm.count("blocks")) 
            {
                serial.writeString("AT+BINF\n");
                std::string answer = serial.readStringUntil("OK\n");
                std::cout << answer << std::endl;
                serial.close();
                return boost::system::errc::success;
            }
            else if (vm.count("reload")) 
            {
                serial.writeString("AT+RST\n");
                serial.readStringUntil("OK\n");
                std::cout << "Completed" << std::endl;
                serial.close();
                return boost::system::errc::success;
            }
            else if (vm.count("unbind")) 
            {
                serial.writeString("AT+BDEL\n");
                serial.readStringUntil("OK\n");
                std::cout << "Completed" << std::endl;
                serial.close();
                return boost::system::errc::success;
            }
        }
        catch(boost::program_options::error& e) 
        { 
            std::cerr << boost::posix_time::to_iso_extended_string(now) 
                      << " ERROR: " << e.what() << std::endl; 
  
            return boost::system::errc::executable_format_error;; 
        } 
        
        serial.writeString("AT+GMR\n");
        std::string answerVersion = serial.readStringUntil("OK\n");

        serial.writeString("AT+BINF\n");
        std::string answerBlocks = serial.readStringUntil("OK\n");
        

        counter.init(setupIni.getCounterSerial(), setupIni.getServerSalt(), now);
        counter.parseVersion(answerVersion);
        counter.parseBlocks(answerBlocks);
        
        if (boost::filesystem::exists("store.dat" ))
        {
            std::cout << "Cashe file exist" << std::endl;
            
            std::ifstream ifs( "store.dat" );
            boost::archive::text_iarchive arLoad( ifs );
       
            // Load the data from cashe
            arLoad & counter;
        }
        
        counter.pushCurrentCount();
        
        counter.sortCount();
        
        //counter.Print();
        
        std::cout << "Clear blocks count" << std::endl;
        
        serial.writeString("AT+BCLR\n");
        serial.readStringUntil("OK\n");
        
        //counter.getIP();
        
        std::string requestToServer;
        counter.makeRequestJSON(requestToServer);
        
        std::cout << "Request : " << requestToServer;
        
        TcpClient client;
        client.connect(setupIni.getServerHost(), setupIni.getServerPort(), boost::posix_time::seconds(10));
        
        boost::posix_time::ptime time_sent = boost::posix_time::microsec_clock::universal_time();
    
        client.write_line(requestToServer, boost::posix_time::seconds(10));
        
        for (;;)            
        {
            std::string line = client.read_line(boost::posix_time::seconds(10));
    
            std::cout << "Answer : " << line << std::endl;
           
            line = boost::remove_erase_if(line, boost::is_any_of("\n\r()"));
           
            boost::regex expr{"^\\d{4}-\\d{2}-\\d{2}T\\d{2}-\\d{2}-\\d{2}!\\d{2}!\\d{2}!\\d{1}$"};
           
            if (boost::regex_match(line, expr))
            {   
                boost::regex exprTail("!\\d{2}!\\d{2}!\\d{1}$");
                
                std::string serverDateTime = boost::regex_replace(line, exprTail, "");
                
                counter.checkSystemTime(serverDateTime);
                break;
            }
        }
        
        boost::posix_time::ptime time_received = boost::posix_time::microsec_clock::universal_time();
    
        std::cout << "Round trip request time: " << (time_received - time_sent).total_milliseconds() << " milliseconds" << std::endl;;        
        
        serial.close();
        
        if (boost::filesystem::exists("store.dat" ))
        {
            std::cout << "Remove cashe file" << std::endl;
            boost::filesystem::remove("store.dat");
        }        
        
    } 
    catch (boost::system::system_error& e)
    {
        std::cerr << boost::posix_time::to_iso_extended_string(now) 
                  << " ERROR: " << e.what() << std::endl;
        
        serial.close();
        
        // Create an cashe file
        std::cout << "Create cashe file" << std::endl;
        std::ofstream ofs( "store.dat" );
        boost::archive::text_oarchive arSave(ofs);
     
        // Save the data
        arSave & counter;
        
        return boost::system::errc::operation_not_permitted;
    }
    catch(std::exception& e) 
    { 
        std::cerr << boost::posix_time::to_iso_extended_string(now) 
                  << " Unhandled Exception reached the top of main: " 
                  << e.what() << ", application will now exit" << std::endl; 
        
        return boost::system::errc::operation_not_permitted;  
    }
    
    return boost::system::errc::success;
}

