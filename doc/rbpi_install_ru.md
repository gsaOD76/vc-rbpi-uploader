### НАСТРОЙКА, ОБНОВЛЕНИЕ

- Поднять превилегию
sudo -s

- Запустить конфигуратор и затем перегрузиться
raspi-config
1. Change user password
4. Localisation options -> I1 Change Locale -> en_US.UTF-8, ru_RU.UTF-8
4. Localisation options -> I2 Change Timezone -> Europe -> Kiev 
5. Interfacing options -> P2 SSH -> Yes
5. Interfacing options -> P5 I2C -> Yes
5. Interfacing options -> P6 Serial -> login -> No -> Hardware -> Yes
7. Advanced Options -> A3 Memory Split -> 16

- Обновить пакеты
sudo apt update && apt upgrade && apt dist-upgrade

- Обновить прошивку и перезагрузиться
sudo rpi-update && reboot

- Посмотреть информацию о сборке системы
lsb_release -a
uname -a

- Проверить температуру
vcgencmd measure_temp

- Выключить использование swap-памяти
sudo dphys-swapfile swapoff
sudo dphys-swapfile uninstall
sudo systemctl disable dphys-swapfile

- Устройство в перезагрузку:
sudo reboot

### ОТКЛЮЧИТЬ WIFI и BLUETOUTH

sudo -s

systemctl disable bluetooth
systemctl stop bluetooth

nano /etc/modprobe.d/raspi-blacklist.conf 

	blacklist brcmfmac
	blacklist brcmutil
	blacklist btbcm
	blacklist hci_uart

nano /boot/config.txt

	dtoverlay=pi3-disable-wifi
	dtoverlay=pi3-disable-bt
	arm_freq=600

- Устройство в перезагрузку:
reboot


### УСТАНОВКА ДОПОЛНИТЕЛЬНОГО ПО

- Установить mc
sudo apt-get install mc

- Установить сервис конфигурирования сетевого экрана
sudo apt-get install iptables-persistent

- Установка тулзы i2c
sudo apt-get install i2c-tools

- Установить git
sudo apt-get install git

- Установить Cmake
sudo apt-get install cmake

- Установить boost
sudo apt-cache search libboost | grep all-dev
sudo apt-get install libboost1.67-*


###  НАСТРОЙКА ФАЙРВОЛ

- Поднять превилигию
sudo -s

- Создать файл конфига сетевого экрана
nano /etc/iptables.rules.sh

		#!/bin/bash

		/sbin/iptables -F
		/sbin/iptables -X

		/sbin/iptables -P INPUT DROP
		/sbin/iptables -P OUTPUT ACCEPT
		/sbin/iptables -P FORWARD DROP

		/sbin/iptables -I INPUT 1 -i lo -p all -j ACCEPT
		/sbin/iptables -I INPUT -s 127.0.0.1  -j ACCEPT

		/sbin/iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
		/sbin/iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

		/sbin/iptables -A INPUT -p tcp --dport 22 -s 0.0.0.0/0 -j ACCEPT

		/sbin/iptables-save > /etc/iptables/rules.v4

		echo "[IP v4]"
		/sbin/iptables -L -n --line-numbers

		/sbin/ip6tables -F
		/sbin/ip6tables -X

		/sbin/ip6tables -P INPUT DROP
		/sbin/ip6tables -P OUTPUT DROP
		/sbin/ip6tables -P FORWARD DROP

		/sbin/ip6tables-save > /etc/iptables/rules.v6

		echo ""
		echo "[IP v6]"
		/sbin/ip6tables -L -n --line-numbers

- Добавить правило запуска
chmod +x /etc/iptables.rules.sh

- Запустить правило
sh /etc/iptables.rules.sh


### УСТАНОВКА ТАЙМЕРА RTC

- Поднять превилигию
sudo -s

- Драйвер I2C
ls /dev/ | grep i2c

- Поискать устройства
i2cdetect -y 1

		     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
		00:          -- -- -- -- -- -- -- -- -- -- -- -- -- 
		10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
		20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
		30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
		40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
		50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
		60: -- -- -- -- -- -- -- -- 68 -- -- -- -- -- -- -- 
		70: -- -- -- -- -- -- -- -- 

- Добавить модули при загрузке
echo i2c-bcm2708 >> /etc/modules
echo i2c-dev >> /etc/modules
echo rtc-ds1307 >> /etc/modules

- Запустить модули
modprobe i2c-bcm2708
modprobe i2c-dev
modprobe rtc-ds1307
echo 'ds1307 0x68' | sudo tee /sys/class/i2c-adapter/i2c-1/new_device

- Проверить текущее время
date

- Отключить фейковые часы
systemctl stop fake-hwclock.service
systemctl disable fake-hwclock.service
apt-get remove fake-hwclock

- Записать текущее время в RTC
hwclock -w

- Устанавливать при старте системное время по RTC часам
nano /etc/rc.local

	echo 'ds1307 0x68' > /sys/class/i2c-adapter/i2c-1/new_device
	sudo hwclock -s
	sudo hwclock -r
	exit 0

- Повторять это каждый час запись в RTC и перезапускать систему в пнд ночью
nano /etc/crontab

		# Reboot every week
		10  0   * * mon root     reboot
		#    
		# Current date time will be stored into the RTC timer 
		* */1   * * *   root    hwclock -w
		#

- Устройство в перезагрузку:
reboot


### УСТАНОВКА CLANG ПРИ НЕОБХОДИМОСТИ
cd ~

wget http://releases.llvm.org/8.0.0/clang+llvm-8.0.0-armv7a-linux-gnueabihf.tar.xz
tar -xf clang+llvm-8.0.0-armv7a-linux-gnueabihf.tar.xz
rm clang+llvm-8.0.0-armv7a-linux-gnueabihf.tar.xz
mv clang+llvm-8.0.0-armv7a-linux-gnueabihf clang_8.0.0

sudo mv clang_8.0.0 /usr/local
export PATH=/usr/local/clang_8.0.0/bin:$PATH
export LD_LIBRARY_PATH=/usr/local/clang_8.0.0/lib:$LD_LIBRARY_PATH
echo 'export PATH=/usr/local/clang_8.0.0/bin:$PATH' >> .bashrc
echo 'export LD_LIBRARY_PATH=/usr/local/clang_8.0.0/lib:$LD_LIBRARY_PATH' >> .bashrc
source .bashrc

clang++ --version


### Cборка ПО

cd ~

- Получить проект из git репозитория
git clone https://gsaOD76@bitbucket.org/gsaOD76/vc-rbpi-uploader.git

- Создать каталоги бинарных файлов и бекап
mkdir bin
mkdir backup

- Зайти в рабочую копию
cd vc-rbpi-uploader

- Запустить формирование make файла
cmake CMakeLists.txt

- Скомпилировать программу
make -j4 -f Makefile

- Скопировать бинарный файл в каталог запуска
cp uploader /home/pi/bin/

- Скопировать конфиг в каталог запуска
cp bin/setup.ini /home/pi/bin/

- Добавить в планировщик
sudo nano /etc/crontab

		# Upload counter data
		*/10 *  * * *   root     cd /home/pi/bin/ && ./uploader >> /dev/null 2>/var/log/rbpi.err
		#*/10 *  * * *   root    cd /home/pi/bin/ && ./uploader >> /var/log/rbpi.log 2>/var/log/rbpi.err 
		#
		# Reset radio module
		* */2   * * *   root     cd /home/pi/bin/ && ./uploader -r >> /dev/null 2>/var/log/rbpi.err
		#
		# Reboot every week
		10  0   * * mon root     reboot
		#  


### СОЗДАНИЕ СКРИПТА ОБНОВЛЕНИЯ ПО

- Перейти в домашний каталог
cd ~

- Создать скрипт
nano update.sh

		#!/bin/bash
		cd /home/pi/vc-rbpi-uploader/
		make -f Makefile clean
		rm Makefile
		find . -iwholename '*cmake*' -not -name CMakeLists.txt -delete
		git pull
		cmake CMakeLists.txt
		make -j4 -f Makefile
		cp uploader /home/pi/bin/

- Дать превелегию на запуск
chmod +x /home/pi/update.sh
